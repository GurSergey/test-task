package TestTask.db.mappers;

import TestTask.entities.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {

    Employee getEmployeeById(Integer id);
    Employee[] getEmployeeByFamilyPattern(String familyPattern);
    Employee[] getAllEmployees(@Param("count") int count, @Param("offset") int offset);
    void addEmployee(Employee employee);

//    List<Employee> getEmployees();

}